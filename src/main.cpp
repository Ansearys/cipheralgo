#include <MyCipherAlgorithm.hpp>
#include <Utility.hpp>

#include <iostream>
#include <fstream>
#include <unistd.h>

/**
 * @brief Affiche à l'écran un manuel d'utilisation
 * @param name Le nom du programme
 */
void printUsage(char* name)
{
    std::cout << "Usage:   " << name << " file -k key [-option] [argument]" << std::endl;
    std::cout << "option:  " << "-k, --key key\t\t The encryption key" << std::endl;
    std::cout << "         " << "-d, --decrypt\t\t Decrypt file" << std::endl;
    std::cout << "         " << "-o, --output file\t The file where to store the result" << std::endl;
    std::cout << "example: " << name << " plaintext.txt -k mykey -o ciphertext.txt" << std::endl;
    std::cout << "         " << name << " ciphertext.txt -k mykey -d -o plaintext.txt" << std::endl;
}

int main(int argc, char* argv[])
{
    MyCipherAlgorithm::Mode mode = MyCipherAlgorithm::Encrypt;
    std::string key, input, output;

    if (argc < 2)
    {
        printUsage(argv[0]);
        return 1;
    }

    // On parse les arguments passés en paramètre lors de l'exécution du programme
    for (int i = 1 ; i < argc ; ++i)
    {
        std::string arg = argv[i];


        if (arg == "-h" || arg == "--help") // Affiche de l'aide
        {
            printUsage(argv[0]);
            return 0;
        }
        else if (arg == "-d" || arg == "--decrypt") // Permet de déchiffrer un fichier
        {
            mode = MyCipherAlgorithm::Decrypt;
        }
        else if (arg == "-o" || arg == "--output") // Sauvegarde le résultat dans un fichier
        {
            if (i + 1 < argc)
            {
                output = argv[++i];

            }
            else
            {
                std::cerr << arg << " option requires one argument." << std::endl;
                return 1;
            }
        }
        else if (arg == "-k" || arg == "--key") // Définit la clé de chiffrement utilisé
        {
            if (i + 1 < argc)
            {
                key = argv[++i];
            }
            else
            {
                std::cerr << arg << " option requires one argument." << std::endl;
                return 1;
            }
        }
        else
        {
            input = argv[i];
        }
    }

    // Le fichier d'entrée est obligtoire
    if (input.empty())
    {
        std::cerr << "no input file to encrypt/decrypt :(" << std::endl;
        printUsage(argv[0]);
        return 1;
    }

    // La clé de chiffrement est obligatoire
    if (key.empty())
    {
        std::cerr << "Please provide a key with --key option" << std::endl;
        printUsage(argv[0]);
        return 1;
    }

    if (!str_isAlphanum(key))
    {
        std::cerr << "Only alphanumeric caracters is allowed for the key" << std::endl;
        printUsage(argv[0]);
        return 1;
    }

    // Lecture du fichier d'entrée
    std::ifstream inputFile(input);
    std::string text;
    if (inputFile)
    {
        std::string line;
        while (!inputFile.eof())
        {
            getline(inputFile, line);
            text += line;

            if (!inputFile.eof())
                text += '\n';
        }
        inputFile.close();
    }
    else
    {
        std::cerr << "Unable to open file " << input << std::endl;
        return 1;
    }

    // Vérifie si le fichier à chiffer est vide
    if (mode == MyCipherAlgorithm::Encrypt && text.empty())
    {
        std::cerr << "Nothing to encrypt" << std::endl;
        return 1;
    }

    // Vérifie que le contenu du fichier peut être déchiffrer
    if (mode == MyCipherAlgorithm::Decrypt && text.length() < MyCipherAlgorithm::BlockSize)
    {
        std::cerr << "This file is not a correct encrypted file" << std::endl;
        return 1;
    }

    // Lance le chiffrement ou le déchiffrement du fichier
    MyCipherAlgorithm cipher;
    std::string result;
    switch (mode)
    {
        case MyCipherAlgorithm::Encrypt:
            result = cipher.encrypt(text, key);
            break;

        case MyCipherAlgorithm::Decrypt:
            result = cipher.decrypt(text, key);
            break;
    }

    // On enregistre le résultat dans un fichier si l'utilisateur l'a demandé
    // Sinon l'affiche dans la console
    if (!output.empty())
    {
        std::ofstream file(output, std::ios::out | std::ios::trunc);
        if (file)
        {
            file << result;
            file.close();
        }
        else
        {
            std::cerr << "Unable to open file " << output << std::endl;
        }
    }
    else
    {
        std::cout << result << std::endl;
    }

    return 0;
}
