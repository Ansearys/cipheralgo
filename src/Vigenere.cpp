#include <Vigenere.hpp>
#include <Utility.hpp>

#include <vector>
#include <algorithm>

namespace Vigenere
{
    std::string encrypt(const std::string& plainText, const std::string& key)
    {
        std::string ciphertext;
        
        // On travaille sur des caractères en majuscules et des chiffres
        std::string upperPlainText = toUpper(plainText);
        std::string upperKey = toUpper(key);
        
        for (unsigned int i = 0 ; i < upperPlainText.length() ; ++i)
        {
            // On ajoute tel quel dans le résultat, les caractères qui ne sont pas alpha-numérique
            if (!isalpha(upperPlainText[i]))
            {
                ciphertext += upperPlainText[i];
                continue;
            }
            
            int ch = ((upperPlainText[i] - 'A') + (upperKey[i % upperKey.length()] - 'A')) % 26;
            ciphertext += (char)(ch + 'A');
        }

        return ciphertext;
    }

    std::string decrypt(const std::string& ciphertext, const std::string& key)
    {
        std::string plainText;
        
        // On travaille sur des caractères en majuscules et des chiffres
        std::string upperCipherText = toUpper(ciphertext);
        std::string upperKey = toUpper(key);
        
        for (unsigned int i = 0 ; i < upperCipherText.size(); ++i)
        {
            // On ajoute tel quel dans le résultat, les caractères qui ne sont pas alpha-numérique
            if (!isalpha(upperCipherText[i]))
            {
                plainText += upperCipherText[i];
                continue;
            }
            
            int ch = ((upperCipherText[i] - 'A') + 26 - (upperKey[i % upperKey.length()] - 'A')) % 26;
            plainText += (char)(ch + 'A');
        }
        
        return plainText;
    }
}
