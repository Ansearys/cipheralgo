#include <MyCipherAlgorithm.hpp>
#include <Vigenere.hpp>
#include <Utility.hpp>

#include <map>
#include <cmath>
#include <ctime>

namespace
{
    const std::map<char, int> PermutationTable =
    {
        {'A', 16}, {'B', 8},  {'C', 18}, {'D', 30}, {'E', 1},  {'F', 17},
        {'G', 11}, {'H', 32}, {'I', 20}, {'J', 4},  {'K', 7},  {'L', 21},
        {'M', 19}, {'N', 26}, {'O', 12}, {'P', 0},  {'Q', 23}, {'R', 5},
        {'S', 29}, {'T', 14}, {'U', 25}, {'V', 6},  {'W', 31}, {'X', 27},
        {'Y', 22}, {'Z', 2},  {'0', 13}, {'1', 15}, {'2', 33}, {'3', 28},
        {'4', 9},  {'5', 10}, {'6', 24}, {'7', 33}, {'8', 35}, {'9', 3},
    };
}

unsigned int MyCipherAlgorithm::BlockSize = 8;

MyCipherAlgorithm::MyCipherAlgorithm() :
    mPlainTextBlocks(),
    mCipherTextBlocks(),
    mEngine()
{
    mEngine.seed(time(nullptr));
}

std::string MyCipherAlgorithm::encrypt(const std::string& plainText, const std::string& key)
{
    initializeBlocks(plainText, Encrypt);
    std::string initialVector = generateRandomInitialVector();

    std::string expandedKey = key;
    expandKey(expandedKey);

    encryptBlock(0, initialVector, expandedKey);
    permuteBlocks(generatePermutationMatrixFromKey(key));

    return generateCipherText(initialVector);
}

std::string MyCipherAlgorithm::decrypt(const std::string& cipherText, const std::string& key)
{
    if (cipherText.length() <= BlockSize)
        return "";

    std::string cipherMessage = cipherText;
    std::string initialVector = extractInitialVector(cipherMessage);

    initializeBlocks(cipherMessage, Decrypt);

    std::string expandedKey = key;
    expandKey(expandedKey);

    permuteBlocks(inverseMatrix(generatePermutationMatrixFromKey(key)));
    decryptBlock(0, initialVector, expandedKey);

    return generatePlainText();
}

void MyCipherAlgorithm::initializeBlocks(const std::string& text, Mode mode)
{
    // On créer le nombre de blocks nécessaire
    mPlainTextBlocks.resize(std::ceil(text.length() / static_cast<float>(BlockSize)));
    mCipherTextBlocks.resize(mPlainTextBlocks.size());

    // Remplissage initiale des blocs
    std::vector<std::string>& blocks = (mode == Encrypt) ? mPlainTextBlocks : mCipherTextBlocks;
    for (unsigned int i = 0 ; i < blocks.size() ; ++i)
        blocks[i] = text.substr(i*BlockSize, BlockSize);
    blocks.back().resize(BlockSize, 'x');
}

void MyCipherAlgorithm::expandKey(std::string& key) const
{
    // On réduit la clé si sa taille dépasse celle d'un bloc
    if (key.length() >= BlockSize)
    {
        key = key.substr(0, BlockSize);
        return;
    }

    // On agrandit la clé dans l'autre cas en la dupliquant
    std::size_t previousKeyLength = key.length();
    key.resize(BlockSize);

    for (unsigned int i = previousKeyLength ; i < BlockSize ; ++i)
        key[i] = key[(i-previousKeyLength) % previousKeyLength];
}

void MyCipherAlgorithm::permuteBlocks(const std::vector<int>& permutations)
{
    std::vector<std::string> newBlocks(permutations.size(), "");
    for (unsigned int i = 0 ; i < permutations.size() ; ++i)
        newBlocks[i] = mCipherTextBlocks[permutations[i]];
    mCipherTextBlocks = newBlocks;
}

void MyCipherAlgorithm::encryptBlock(std::size_t blockNumber, const std::string& input, const std::string& key)
{
    if (blockNumber == mPlainTextBlocks.size())
        return;

    std::string cipherText = Vigenere::encrypt(input, key);
    mCipherTextBlocks[blockNumber] = str_xor(mPlainTextBlocks[blockNumber], cipherText);

    encryptBlock(++blockNumber, cipherText, key);
}

void MyCipherAlgorithm::decryptBlock(std::size_t blockNumber, const std::string& input, const std::string& key)
{
    if (blockNumber == mCipherTextBlocks.size())
        return;

    std::string plainText = Vigenere::encrypt(input, key);
    mPlainTextBlocks[blockNumber] = str_xor(mCipherTextBlocks[blockNumber], plainText);

    decryptBlock(++blockNumber, plainText, key);
}

std::string MyCipherAlgorithm::generateRandomInitialVector()
{
    std::string alphabet =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "0123456789";

    std::uniform_int_distribution<int> distrib(0, alphabet.length());

    std::string initialVector;
    for (unsigned int i = 0 ; i < BlockSize ; ++i)
        initialVector += alphabet[distrib(mEngine)];

    return initialVector;
}

std::string MyCipherAlgorithm::extractInitialVector(std::string& cipherText) const
{
    std::string initialVector = cipherText.substr(0, BlockSize);
    cipherText.erase(0, BlockSize);

    return initialVector;
}

std::string MyCipherAlgorithm::generateCipherText(const std::string& initialVector) const
{
    std::string cipherText = initialVector;
    for (const std::string& block : mCipherTextBlocks)
        cipherText += block;

    return cipherText;
}

std::string MyCipherAlgorithm::generatePlainText() const
{
    std::string plainText;
    for (const std::string& block : mPlainTextBlocks)
        plainText += block;

    return plainText;
}

std::vector<int> MyCipherAlgorithm::generatePermutationMatrixFromKey(const std::string& key)
{
    std::vector<int> permutations(mPlainTextBlocks.size(), -1);

    int keyIndex = 0;
    std::string upperKey = toUpper(key);

    for (unsigned int i = 0 ; i < mPlainTextBlocks.size() ; ++i)
    {
        // On récupère la valeur associé à chaque lettre de la clé dans la table de permutation
        // pour générer une matrice de permutation
        unsigned int pos = PermutationTable.at(upperKey[keyIndex]) % permutations.size();

        // Si on tombe sur un case de la matrice qui a déjà été assigné, on se déplace vers la droite
        // jusqu'a tomber sur la 1er case vide
        while (permutations[pos] != -1)
        {
            ++pos;
            if (pos == permutations.size())
                pos = 0;
        }

        permutations[pos] = i;

        keyIndex = (keyIndex + 1) % upperKey.length();
    }

    return permutations;
}
