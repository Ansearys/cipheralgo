#include <Utility.hpp>
#include <cassert>

std::string toUpper(const std::string& str)
{
    std::string result;
    for (unsigned int i = 0 ; i < str.length() ; ++i)
        result += std::toupper(str[i]);

    return result;
}

std::vector<int> inverseMatrix(const std::vector<int> matrix)
{
    std::vector<int> inverseMatrix(matrix.size(), -1);

    for (unsigned int i = 0 ; i < matrix.size() ; ++i)
        inverseMatrix[matrix[i]] = i;

    return inverseMatrix;
}

std::string str_xor(const std::string& str1, const std::string& str2)
{
    assert(str1.length() == str2.length());

    std::string result;
    for (unsigned int i = 0 ; i < str1.length() ; ++i)
            result += str1[i] ^ str2[i];

    return result;
}

bool str_isAlphanum(const std::string& str)
{
    for (unsigned int i = 0 ; i < str.length() ; ++i)
    {
        if (!isalnum(str[i]))
            return false;
    }

    return true;
}
