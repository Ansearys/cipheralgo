Groupe: 
Florian Gadal
Etienne Gourinchas

* Pr�requis: 
	- Compilateur C++ compatible avec la norme c++11
	- CMake 3.0 ou sup�rieur (https://cmake.org/download/)
* Cr�er un dossier build � la racine du projet
* Lancer une invite de commande � l'int�rieur de ce dossier et lancer la commande: cmake .. -G "MinGW Makefiles" (Pour Linux: cmake .. -G "Unix Makefiles")
* Lancer ensuite la commande mingw32-make (Pour linux: make)
* L'ex�cutable se trouve dans le dossier bin/ � l'int�rieur du dossier build/
* Lancer la commande "cipheralgo.exe" pour afficher une aide pour utiliser le programme