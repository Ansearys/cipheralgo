#include <bitset>
#include <array>
#include <vector>
#include <random>

/**
 * @class MyCipherAlgorithm
 * @author Florian Gadal, Etienne Gourinchas 
 * @file MyCipherAlgorithm.hpp
 * @brief Cette classe permet de mettre en oeuvre notre algorithme de chiffrement
 */
class MyCipherAlgorithm
{   
public:
    enum Mode
    {
        Encrypt,
        Decrypt,
    };
    
public:
    /**
     * @brief Constructeur par défaut
     */
    MyCipherAlgorithm();
    
    /**
     * @brief Chiffre une chaine de caractère à l'aide d'une clé
     * @param plainText La chaine de caractère à chiffrer
     * @param key Le clé de chiffrement
     * @return la chaine de cractère chiffrée
     */
    std::string encrypt(const std::string& plainText, const std::string& key);
    
    /**
     * @brief Déchiffre une chaine de caractère à l'aide d'une clé
     * @param cipherText la chaine de caractère chiffrée par l'algorithme
     * @param key La clé de déchiffrement
     * @return la chaine de caractère déchiffrée
     */
    std::string decrypt(const std::string& cipherText, const std::string& key);

private:
    /**
     * @brief Crée les blocs et les initialise avec les données de départ
     * @param text Le texte initial
     * @param mode Le mode en cours d'utilisation (Chiffrement ou déchiffrement)
     */
    void initializeBlocks(const std::string& text, Mode mode);
    
    /**
     * @brief Etend la clé à la taille d'un bloc si celle ci est trop petite
     * @param key La clé de chiffrement
     */
    void expandKey(std::string& key) const;
    
    /**
     * @brief Permute les blocs en fonction d'une matrice de permutation
     * @param permutations La matrice de permutation
     */
    void permuteBlocks(const std::vector<int>& permutations);
    
    /**
     * @brief Chiffre un bloc du schéma OFB
     * @param blockNumber Le numéro du bloc à déchiffrer
     * @param input Les données d'entrées du bloc actuel
     * @param key La clé de chiffrement
     */
    void encryptBlock(std::size_t blockNumber, const std::string& input, const std::string& key);
    
    /**
     * @brief Déchiffre un bloc du schéma OFB
     * @param blockNumber Le numéro du bloc à déchiffrer
     * @param input Les données d'entrées du bloc actuel
     * @param key La clé de chiffrement
     */
    void decryptBlock(std::size_t blockNumber, const std::string& input, const std::string& key);
    
    /**
     * @brief Extrait le vecteur initial du texte chiffré
     * @param cipherText Le texte chiffré
     * @return Une chaine de caractère représentant le vecteur initial
     */
    std::string extractInitialVector(std::string& cipherText) const;
    
    /**
     * @brief Génère un vecteur initial aléatoire
     * @return Le vecteur initial généré
     */
    std::string generateRandomInitialVector();
    
    /**
     * @brief Génère le texte final chiffré en fusionnant tous les blocs et en préfixant le message chiffré du vecteur d'initialisation
     * @param initialVector Le vecteur d'initialisation
     * @return Le texte chiffré
     */
    std::string generateCipherText(const std::string& initialVector) const;
    
    /**
     * @brief Génère le texte final déchiffré en fusionnant tous les blocs
     * @return Le texte déchiffré
     */
    std::string generatePlainText() const;
    
    /**
     * @brief Génère une matrice de permutation en fonction de la clé de chiffrement
     * @param key La clé de chiffrement
     * @return La matrice de permutation
     */
    std::vector<int> generatePermutationMatrixFromKey(const std::string& key);
    
public:
    static unsigned int BlockSize;

private:   
    std::vector<std::string> mPlainTextBlocks;
    std::vector<std::string> mCipherTextBlocks;
    
    std::string mInitialVector;
    
    std::default_random_engine mEngine;
};
