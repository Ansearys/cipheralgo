#include <string>
#include <vector>

/**
 * @brief Transforme une chaine de caractère en majuscule
 * @param str La chaine de caractère à transformer
 * @return La nouvelle chaine de caractère en majuscule
 */
std::string toUpper(const std::string& str);

/**
 * @brief Inverse une matrice de permutation
 * @param matrix La matrice de permutation
 * @return La matrice de permutation inversée
 */
std::vector<int> inverseMatrix(const std::vector<int> matrix);

/**
 * @brief Effectue un XOR entre 2 chaines de caractère
 * @param str1 La 1ère chaine de caractère
 * @param str2 La seconde chaine de caractère
 * @return str1 XOR str2
 */
std::string str_xor(const std::string& str1, const std::string& str2);

/**
 * @brief Indique si la chaine de caractère ne comporte que des caractère alpha-numérique
 * @param str La chaine de caractère à tester
 */
bool str_isAlphanum(const std::string& str);
