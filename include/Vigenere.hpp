#include <string>

namespace Vigenere
{
    /**
     * @brief Chiffre une chaine de caractère à l'aide de l'algorithme de Vigenère
     * @param message Le message à chiffrer
     * @param key La clé de chiffrement
     * @return Le message chiffré
     */
    std::string encrypt(const std::string& message, const std::string& key);
    
    /**
     * @brief Déchiffre une chaine de caractère à l'aide de l'algorithme de Vigenère
     * @param message Le message à déchiffrer
     * @param key La clé de chiffrement
     * @return Le message déchiffrer
     */
    std::string decrypt(const std::string& message, const std::string& key);
}
